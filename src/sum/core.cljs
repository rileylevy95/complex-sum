(ns ^:figwheel-hooks sum.core
  (:refer-clojure :exclude [ + - * /])
  (:require
   [cljs.pprint :refer [cl-format]]
   [webjunk.bulma :as bulma]
   [webjunk.complex :as complex :refer [complex + - * / abs]]
   [webjunk.svg :as svg]

   [clojure.core.async :as async :refer [chan go go-loop put! take! <! >! alt!]]

   [goog.dom :as gdom]
   [reagent.core :as reagent :refer [atom]]
   [reagent.dom :as rdom]))


;; define your app data so that it doesn't get over-written on reload
(defonce state (atom {:z (complex 2 1)
                      :start (complex)
                      :vecs []}))

(def inputs (chan (async/sliding-buffer 10)))

(defn fmt [& args] (apply cl-format nil args))

(defn get-app-element []
  (gdom/getElement "app"))

(def inf 10)
(def grid
  (for [t (range -10 11)]
    ^{:key [:grid t]}
    [:<>
     (svg/line {:class "grid"} [t (- inf)] [t inf])
     (svg/line {:class "grid"} [(- inf) t] [inf t])]))
(def axes
  [:<>
   (svg/line {:class "axis"} [0 (- inf)] [0 inf])
   (svg/line {:class "axis"} [(- inf) 0] [inf 0])
   [:g {:transform "translate(8.3 .6) scale(1 -1)"}
    (svg/text {:dz [.5 .5] :class "axis"} "Re")]
   [:g {:transform "translate(-.4 9.5) scale(1 -1)"}
    (svg/text {:dz [.5 .5] :class "axis"} "Im")]])

(defn ->svg-coords [^js/SVGSVGElement svg x y]
  (let [p (.createSVGPoint svg)
        mat (-> svg .getScreenCTM .inverse)]
    (set! (.-x p) x)
    (set! (.-y p) y)
    (let [p (.matrixTransform p mat)]
      (complex (.-x p) (.-y p)))))

(def put-it
  (memoize
   (fn put-it-unmemo [tag]
     #(put! inputs
            [tag
             (->svg-coords (gdom/getElement "diagram")
                           (.-clientX %)
                           (.-clientY %))]))))

(defn handle-resized [& _]
  (let [x js/window]
    (swap! state assoc
           :width  (.-innerWidth x)
           :height (.-innerHeight x))))
(handle-resized)
(set! (-> js/window .-onresize) handle-resized)

(defn keyize [z]
  (str (complex/real z)
       " "
       (complex/imag z)))

(def deltas
  (memoize
   (fn [vecs]
     (for [[z0 z1] vecs]
       (- z1 z0)))))
(def draw-vecs
  (memoize
   (fn [vecs]
     (for [[z0 z1 :as k] vecs]
       (svg/line {:class "arrowed" :key (keyize z0)} z0 z1)))))

(defn sum []
  [:main.columns.is-centered.is-multiline
   [:div.column.is-narrow
    [:div {:on-mouse-move (put-it :move)
           :on-click (put-it :click)}
     [:svg.diagram {:id "diagram"
                    :viewBox "-10 -10 20 20"
                    :width (min (:width @state)
                                (:height @state))}
      [:defs [:marker {:class "arrow"
                       :id "arrow"
                       :viewBox "-5 -5 10 10"
                       :orient "auto"
                       :markerWidth  10
                       :markerHeight 10
                       :refX 4
                       :refY 0}
              [:path {:d "M0,0 L-2,3 L5,0 L-2,-3 Z"}]]]
      grid axes
      (draw-vecs (:vecs @state))
      (svg/line {:class "arrowed current"} (:start @state) (:z @state))
      (svg/line {:id "sum" :class "arrowed"} (complex) (:z @state))
      ]]]
   [:div.column
    (let [deltas (deltas (:vecs @state))]
      [:table.table {:id "ledger"}
       [:tbody
        [:tr [:th] [:th "re"] [:th "im"]]
        (for [[x y :as z] deltas]
          [:tr {:key (keyize z)} [:td] [:td (fmt "~,3F" x)] [:td (fmt "~,3@Fi" y)]])
        (let [[x y :as z] (- (:z @state) (:start @state))]
          [:tr [:th.current "current:"] [:td.current (fmt "~,3F" x)] [:td.current (fmt "~,3@Fi" y)]])
        (let [[x y] (:z @state)]
          [:tr
           [:th.sum "sum: "]
           [:td.sum (fmt "~,3F " x)]
           [:td.sum (fmt "~,3@F" y)]])]])]])

(go-loop []
  (let [[tag z] (<! inputs)]
    (case tag
      :click
      (do (swap! state update :vecs conj [(:start @state) z])
          (swap! state assoc :start z))
      :move (swap! state assoc :z z))
    (recur)))


(defn mount [el]
  (rdom/render [sum] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
  )
